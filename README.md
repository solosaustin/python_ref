# python reference by example

Quick Start
-----------
### install
### hello world

basic
-----------
### data type
> * int
> * float
> * complex
> * bool
> * string
> * None
### operators
### list & tuple
### string
### dict & set

### condition
### loop

function
-----------

### def
### arguments
### variable scope
### lambda

advance
-----------
### slice
### iterator
### generator

[comment]: <> (functional programming)

model
-----------
### import

OOP
-----------

### Class & Object
### Encapsulation
### Inheritance
### Polymorphism

[comment]: <> (advance oop)

error debug test
-----------
### error handling
### debug
### unit test